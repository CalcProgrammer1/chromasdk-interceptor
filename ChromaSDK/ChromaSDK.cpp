// ChromaSDK.cpp : Defines the exported functions for the DLL application.
// This DLL project is a mock implementation of the Razer Chroma SDK for integrating non-Razer peripherals with Razer Chroma Applications
// It should also be able to forward calls to the real Razer Chroma SDK

#include <Windows.h>
#include "RzChromaSDK.h"
#include "RzChromaSDKDefines.h"
#include "RzChromaSDKTypes.h"
#include "RzErrors.h"
#include "LEDStrip.h"

// Location of real Chroma SDK DLL files
#ifdef _WIN64
#define CHROMASDKDLL        "C:\\Program Files\\Razer Chroma SDK\\bin\\RzChromaSDK64.dll"
#else
#define CHROMASDKDLL        "C:\\Program Files (x86)\\Razer Chroma SDK\\bin\\RzChromaSDK.dll"
#endif

// Pointers to real Chroma SDK DLL functions
typedef RZRESULT(*INIT)(void);
typedef RZRESULT(*UNINIT)(void);
typedef RZRESULT(*CREATEEFFECT)(RZDEVICEID DeviceId, ChromaSDK::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId);
typedef RZRESULT(*CREATEKEYBOARDEFFECT)(ChromaSDK::Keyboard::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId);
typedef RZRESULT(*CREATEMOUSEEFFECT)(ChromaSDK::Mouse::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId);
typedef RZRESULT(*CREATEMOUSEPADEFFECT)(ChromaSDK::Mousepad::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId);
typedef RZRESULT(*CREATEHEADSETEFFECT)(ChromaSDK::Headset::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId);
typedef RZRESULT(*CREATECHROMALINKEFFECT)(ChromaSDK::ChromaLink::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId);
typedef RZRESULT(*CREATEKEYPADEFFECT)(ChromaSDK::Keypad::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId);
typedef RZRESULT(*DELETEEFFECT)(RZEFFECTID EffectId);
typedef RZRESULT(*SETEFFECT)(RZEFFECTID EffectId);

INIT                    RzInit;
UNINIT                  RzUnInit;
CREATEEFFECT            RzCreateEffect;
CREATEKEYBOARDEFFECT    RzCreateKeyboardEffect;
CREATEMOUSEEFFECT       RzCreateMouseEffect;
CREATEMOUSEPADEFFECT    RzCreateMousepadEffect;
CREATEHEADSETEFFECT     RzCreateHeadsetEffect;
CREATEKEYPADEFFECT      RzCreateKeypadEffect;
CREATECHROMALINKEFFECT  RzCreateChromaLinkEffect;
DELETEEFFECT            RzDeleteEffect;
SETEFFECT               RzSetEffect;

HMODULE hModule = NULL;
LEDStrip strip;
LEDStrip strip2;

__declspec(dllexport) RZRESULT Init(void)
{
    hModule = LoadLibrary(CHROMASDKDLL);

    if (hModule)
    {
        RzInit = (INIT)GetProcAddress(hModule, "Init");
        if (RzInit)
        {
            //Initialize the SDK
            RzInit();

            RzUnInit                    = (UNINIT)GetProcAddress(hModule, "UnInit");
            RzCreateEffect              = (CREATEEFFECT)GetProcAddress(hModule, "CreateEffect");
            RzCreateKeyboardEffect      = (CREATEKEYBOARDEFFECT)GetProcAddress(hModule, "CreateKeyboardEffect");
            RzCreateMouseEffect         = (CREATEMOUSEEFFECT)GetProcAddress(hModule, "CreateMouseEffect");
            RzCreateMousepadEffect      = (CREATEMOUSEPADEFFECT)GetProcAddress(hModule, "CreateMousepadEffect");
            RzCreateHeadsetEffect       = (CREATEHEADSETEFFECT)GetProcAddress(hModule, "CreateHeadsetEffect");
            RzCreateKeypadEffect        = (CREATEKEYPADEFFECT)GetProcAddress(hModule, "CreateKeypadEffect");
            RzCreateChromaLinkEffect    = (CREATECHROMALINKEFFECT)GetProcAddress(hModule, "CreateChromaLinkEffect");
            RzDeleteEffect              = (DELETEEFFECT)GetProcAddress(hModule, "DeleteEffect");
            RzSetEffect                 = (SETEFFECT)GetProcAddress(hModule, "SetEffect");

            strip.Initialize("COM1", 0, 0, 0, 0, 0, 0, 0);
            strip.SetNumLEDs(15, 0, 0, 0, 0, 0, 0, 0);
            strip2.Initialize("COM2", 0, 0, 0, 0, 0, 0, 0);
            strip2.SetNumLEDs(30, 0, 0, 0, 0, 0, 0, 0);
        }
    }

    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT UnInit(void)
{
    RzUnInit();
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT CreateEffect(RZDEVICEID DeviceId, ChromaSDK::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId)
{
    RzCreateEffect(DeviceId, Effect, pParam, pEffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT CreateKeyboardEffect(ChromaSDK::Keyboard::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId)
{
    RzCreateKeyboardEffect(Effect, pParam, pEffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT CreateMouseEffect(ChromaSDK::Mouse::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId)
{
    RzCreateMouseEffect(Effect, pParam, pEffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT CreateHeadsetEffect(ChromaSDK::Headset::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId)
{
    RzCreateHeadsetEffect(Effect, pParam, pEffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT CreateMousepadEffect(ChromaSDK::Mousepad::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId)
{
    RzCreateMousepadEffect(Effect, pParam, pEffectId);

    ChromaSDK::Mousepad::CUSTOM_EFFECT_TYPE FireflyEffect = {};
    unsigned int colors[30];
    memcpy(&FireflyEffect, pParam, sizeof(FireflyEffect));

    for (int i = 0; i < 30; i++)
    {
        colors[i] = FireflyEffect.Color[0];
    }

    strip.SetLEDs(colors);
    strip2.SetLEDs(colors);

    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT CreateKeypadEffect(ChromaSDK::Keypad::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId)
{
    RzCreateKeypadEffect(Effect, pParam, pEffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT CreateChromaLinkEffect(ChromaSDK::ChromaLink::EFFECT_TYPE Effect, PRZPARAM pParam, RZEFFECTID *pEffectId)
{
    RzCreateChromaLinkEffect(Effect, pParam, pEffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT DeleteEffect(RZEFFECTID EffectId)
{
    RzDeleteEffect(EffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT SetEffect(RZEFFECTID EffectId)
{
    RzSetEffect(EffectId);
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT RegisterEventNotification(HWND hWnd)
{
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT UnregisterEventNotification()
{
    return RZRESULT_SUCCESS;
}

__declspec(dllexport) RZRESULT QueryDevice(RZDEVICEID DeviceId, DEVICE_INFO_TYPE &DeviceInfo)
{
    return RZRESULT_SUCCESS;
}